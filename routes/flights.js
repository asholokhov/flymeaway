var mysql = require('mysql');
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'halflife',
    database : 'flymeup'
});

connection.connect(function(err) {
    if (err) throw err;

    // connection successful
    console.log('Connection to DB established.');
});


exports.list = function(req, res) {
    // list all tickets
    var sql = 'SELECT \
                FLIGHTS.id, \
                    C1.name AS c_from, \
                    C2.name AS c_to, \
                    FLIGHTS.date AS f_day, \
                    FLIGHTS.price, \
                    FLIGHTS.ticket_count AS tickets, \
                    companies.name AS company, \
                    companies.rating \
                FROM flights AS FLIGHTS \
                INNER JOIN cities AS C1 \
                ON FLIGHTS.from_id = C1.id \
                INNER JOIN cities AS C2 \
                ON FLIGHTS.to_id = C2.id \
                INNER JOIN companies \
                ON FLIGHTS.company_id = companies.id;';

    connection.query(sql, function(err, result) {
        if (err) throw err;
        console.log(result);
        res.render('find', { title: 'Flights count', tickets: result });
    });
};

exports.getCompanies = function(req, res) {
    connection.query('SELECT * FROM companies', function(err, result) {
        if (err) throw err;

        // return companies list as JSON
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(result));
    });
};

exports.getCities = function(req, res) {
    connection.query('SELECT * FROM cities', function(err, result) {
        if (err) throw err;

        // return cities list as JSON
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(result));
    });
};

exports.buyTicket = function(req, res) {
    var id = req.body.tickId;
    connection.query('UPDATE flights SET ticket_count = ticket_count - 1 WHERE id=?', [id], function(err, result) {
        if (err) throw err;
        res.end(JSON.stringify({result: true}));
    })
};

exports.filterTickets = function(req, res) {
    // req.query contain all json params
    var sql = 'SELECT flights.id, \
                    C1.name AS c_from, \
                    C2.name AS c_to, \
                    flights.date AS f_day, \
                    flights.price, \
                    flights.ticket_count AS tickets, \
                    companies.name AS company, \
                    companies.rating \
                FROM flights \
                INNER JOIN cities AS C1 ON flights.from_id = C1.id \
                INNER JOIN cities AS C2 ON flights.to_id = C2.id \
                INNER JOIN companies ON flights.company_id = companies.id \
                WHERE locate(?, flights.date) > 0 \
                AND flights.from_id = ? \
                AND flights.to_id = ? AND flights.price <= ? \
                AND companies.id = ?;';

    var params = req.query;
    connection.query(sql, [params.time, params.fromId, params.toId, params.maxPrice, params.compId], function(err, result) {
        if (err) throw err;

        res.render('t_list', { title: 'Flights count', tickets: result });
    });
};