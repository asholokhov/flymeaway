$(document).ready(function() {

    // process links
    var currentPath = window.location.pathname;
    $('.nav')
        .find('a')
        .each(function() {
            if ($(this).attr('href') === currentPath) {
                $(this)
                    .parent()
                    .addClass('active');
            }
        });

    // load filter fields from server by AJAX request
    if (currentPath === '/find') {

        // date picker
        $("#dpdate")
            .attr("value", moment().format("DD.MM.YYYY"))
            .datepicker({ format: 'dd.mm.yyyy' });

        // get companies
        var compList = $('#comp-list');
        $.ajax({
            type: 'GET',
            url: '/getCompanies',
            dataType: 'json'
        }).done(function(data) {
            for (var i = 0; i < data.length; i++) {
                var html = '<option id="comp_#id">#name</option>'
                    .replace('#id', data[i].id)
                    .replace('#name', data[i].name);

                $(html).appendTo(compList);
            }
        }).fail(function(jqXHR, textStatus, errorThrown)  {
            console.log(textStatus, errorThrown);
            throw errorThrown;
        });

        // get from and to fields
        var fromList = $('#from-list');
        var toList = $('#to-list');
        $.ajax({
            type: 'GET',
            url: '/getCities',
            dataType: 'json'
        }).done(function(data) {
            for (var i = 0; i < data.length; i++) {
                var html = '<option id="city_#id">#name</option>'
                    .replace('#id', data[i].id)
                    .replace('#name', data[i].name);

                $(html).appendTo(fromList);
                $(html).appendTo(toList);
            }
        }).fail(function(jqXHR, textStatus, errorThrown)  {
            console.log(textStatus, errorThrown);
            throw errorThrown;
        });

        // apply button action
        $('#search-btn').on('click', function() {
            var btn = $(this);
            btn.button('loading');

            $.ajax({
                type: 'GET',
                url: '/applyFilter',
                data: {
                    fromId: $('#from-list option:selected').attr('id').split('_')[1],
                    toId: $('#to-list option:selected').attr('id').split('_')[1],
                    compId:$('#comp-list option:selected').attr('id').split('_')[1],
                    maxPrice: $("#price").val(),
                    time: $("#dpdate").val().split('.').reverse().join('-')
                }
            }).done(function (data) {
                $("#ticket-list").html(data);
            }).fail(function(jqXHR, textStatus, errorThrown)  {
                console.log(textStatus, errorThrown);
                throw errorThrown;
            }).always(function() {
                btn.button('reset');
            });
        });

        // buy tickets
        $(".js-buy-btn").on('click', function() {
            var st = $(window).scrollTop();
            var tick = $(this);
            var id = $(this).attr('id').split('_')[1];
            $.ajax({
                type: 'POST',
                url: '/buyTicket',
                dataType: 'json',
                data: {
                    tickId: id
                }
            }).done(function(data) {
                if (data.result) {
                    var badge = $('#tick_' + id).parent().find('.js-tcount');
                    var count = parseInt(badge.text());
                    badge.text(count - 1);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                throw errorThrown;
            }).always(function() {
                $(window).scrollTop(st);
            });
        });
    }
});